<!DOCTYPE html>  
<html>  
<head>  
    <meta content="text/html; charset=utf-8">  
    <title>Ex1 answers-git</title>	
</head>  
<body>  
<h1>Ex1 solution - Ben Yair and Daniel Elias<br></h1>
<?php
	
	
	class customer
	{
		protected $name;
		protected $eMail;
		protected $phoneNum;
		public static $counter = 0;
		
		public function __construct($_name,$_email,$_phonenum)
		{	
			$this->name = $_name;
			$this->eMail = $_email;
			$this->phoneNum = $_phonenum;
			
			self::$counter++;
		}
		public function __destruct()
		{
			self::$counter--;
		}
		public function show()
		{
			
			$this->tableHeder = '<table border = "1">  
			<tr>  
			   	<td><b>customer name</b></td>  
				<td><b>Mail</b></td>  
				<td><b>Phone Number</b></td>  
			</tr>';  
					  
			
			$this->table= '<tr>  
				 <td>'.$this->name. '</td>  
				 <td>'.$this->eMail. '</td>  
				 <td>'.$this->phoneNum. '</td>  
				 </tr>  
				 </table>';

			
			return $this->tableHeder.$this->table;
		}
		public static function getcoubter()
		{
			return 'Number of active customer is: '. self::$counter.'<br>';
		}
	}	
	class vipcustomer extends customer 
    { 
        protected $loyaltyPoints; 
        public static $vipcounter = 0; 
         
        public function __construct($_name,$_email,$_phonenum,$_points) 
        { 
             
            parent::__construct($_name,$_email,$_phonenum); 
            customer::$counter--; 
            if(isset($_points)) 
            { 
                $this->loyaltyPoints = $_points; 
            } 
            else 
            { 
                echo 'You didnt set the points to the customer'; 
            } 
            self::$vipcounter++; 
        } 
         
        public function __destruct() 
        { 
            self::$vipcounter--; 
        } 
         
        public function show() 
        { 
            parent::show(); 
            $vip_table_header = str_replace('</tr>','<td><b>VIP Loyalty Points</b></td></tr>',$this->tableHeder); 
            $vip_table = str_replace('</tr>','<td>'.$this->loyaltyPoints.'</td></tr>',$this->table); 
            return $vip_table_header.$vip_table; 
        } 
         
        public static function getvipcoubter() 
        { 
            return 'Number of active vip customers is: '.self::$vipcounter; 
        } 
         
    } 
	$customer1 = new customer('ben','ben@gmail.com','050-0000000');
	$customer2 = new customer('omer','omer@gmail.com','050-1111111');
	$vipcustomer1 = new vipcustomer('aviv','aviv@gmail.com','050-11221122',100);
	
	echo $customer1->show();
	
	echo $vipcustomer1->show(); 
    unset($customer2); 
    echo customer::getcoubter(); 
    echo vipcustomer::getvipcoubter(); 
?>
		
</body>  
</html>